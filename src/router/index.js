import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView from '../views/homeCom/HomeView'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: HomeView
  },
  {
    path:"/detailsCom",
    name: 'detailsCom',
    component:()=>import('../views/detailsCom/detailsView.vue')
  },
  {
    path:"/loginCom",
    name: 'loginCom',
    component:()=>import('../views/loginCom/loginView.vue')
  },
  {
    path:"/FclassCom",
    name: 'FclassCom',
    component:()=>import('../views/FclassCom/FclassView.vue')
  },
  {
    path:"/Eatcom",
    name: 'Eatcom',
    component:()=>import('../views/EatCom/EatView.vue')
  },
  {
    path:"/ShoppingCom",
    name: 'ShoppingCom',
    component:()=>import('../views/ShoppingCom/ShoppingView.vue')
  },
  {
    path:"/ICom",
    name: 'ICom',
    component:()=>import('../views/ICom/IView.vue')
  }
 
]


const router = new VueRouter({
  routes
})


export default router
