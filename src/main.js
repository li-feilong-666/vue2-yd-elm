import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vant from './vant'
import rem from './utils/rem'
Vue.config.productionTip = false
// 引入axios封装的请求接口
import api from '@/api/user'
Vue.prototype.$http = api;
import mymixin from "@/utils/mymixin";
require('@/mock/index')
Vue.mixin(mymixin)
new Vue({
  router,
  store,
  vant,
  rem,
  render: h => h(App)
}).$mount('#app')
