const Mock = require('mockjs')
//使用mock模拟数据
Mock.mock('/data/regist', 'post', res => {
    console.log(res);
    let tel=JSON.parse(res.body).tel
    
    let pas=JSON.parse(res.body).password
    let reg_tel=/^(13[0-9]|14[01456879]|15[0-35-9]|16[2567]|17[0-8]|18[0-9]|19[0-35-9])\d{8}$/
   console.log(reg_tel.test(tel));
   console.log(pas.length>=6);
    if ( tel&& pas) {
        //校验手机号是否正确
        if (reg_tel.test(tel)&& pas.length>=6) {
            return {
                data: {
                    code: 200,
                    success: "注册成功"
                }
            }
        } else {
            return {
                data: {
                    code: 300,
                    success: "注册失败"
                }
            }
        }
    }
    return {
        data: {
            code: 400,
            success: "注册失败"
        }
    }
})

//登录
Mock.mock('/data/login', 'post', res => {
    console.log(res);
    let sms=JSON.parse(res.body).sms
    let yzm=JSON.parse(res.body).yzm
    let datas=JSON.parse(localStorage.getItem('token'))
    if ( sms&& yzm) {
        //校验手机号是否存在
        if (sms==datas.tel&& yzm==datas.password) {
            return {
                data: {
                    code: 200,
                    success: "登录成功"
                }
            }
        } else {
            return {
                data: {
                    code: 300,
                    success: "登录失败"
                }
            }
        }
    }
    return {
        data: {
            code: 400,
            success: "登录失败"
        }
    }
})