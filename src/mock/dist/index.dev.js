"use strict";

var Mock = require('mockjs'); //使用mock模拟数据


Mock.mock('/data/login', 'post', function (res) {
  if (JSON.parse(res.body).sms && JSON.parse(res.body).yzm) {
    return {
      data: {
        code: 200,
        success: "请求成功"
      }
    };
  } else if (JSON.parse(res.body).password && JSON.parse(res.body).tel) {
    return {
      data: {
        code: 200,
        success: "注册成功"
      }
    };
  }

  return {
    data: {
      code: 400,
      success: "发送失败"
    }
  };
});