import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"
import getHome from './getHome'
import shopping from './shopping'
Vue.use(Vuex)

export default new Vuex.Store({

  state:{
    islogin:false// 登录成功之后变为true
  },
  // state: {
  //   images: [],
  //   gbjpg: {},
  //   navlist: [],
  //   navitem: [],
  //   allList: [],
  // },
  // getters: {
  // },
  // mutations: {
  //   imagesFn(state,value){
  //     state.images=value
  //   },
  //   gbjpgFn(state,value){
  //     state.gbjpg=value
  //   },
  //   navlistFn(state,value){
  //     state.navlist=value
  //   },
  //   navitemFn(state,value){
  //     state.navitem=value
  //   },
  //   allListFn(state,value){
  //     state.allList=value
  //   }
  // },
  // actions: {
  //   async getHomeApi(context) {
  //     let HomeView = await axios.get('/data/homeApi.json')
  //     context.commit('imagesFn', HomeView.data.data.list[0].icon_list)
  //     context.commit('gbjpgFn', HomeView.data.data.list[1])
  //     context.commit('navlistFn',HomeView.data.data.list[2].icon_list)
  //     context.commit('navitemFn',HomeView.data.data.list[3].product_list)
  //     context.commit('allListFn',HomeView.data.data.list[12].product_list)
  //   }

  // },
  plugins: [createPersistedState({
    key:"islogin",
    value:["islogin"]
  })],
  modules: {
    // 首页数据
    getHome,
    shopping,
  }
})
