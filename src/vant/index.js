import Vue from 'vue';
import { Tabbar, TabbarItem,Icon,Search,Lazyload,Swipe, SwipeItem,Grid, GridItem,CountDown,Tab, Tabs,Col, Row
,Sticky 
,Sidebar, SidebarItem ,
Popup ,
GoodsAction, GoodsActionIcon, GoodsActionButton,Toast,Dialog ,Form ,Field,Button,Divider,Card
,Checkbox, CheckboxGroup ,Tag    ,SubmitBar  
} from 'vant';
Vue.use(Card);
Vue.use(Tag);
Vue.use(SubmitBar);

Vue.use(Checkbox);
Vue.use(CheckboxGroup);
Vue.use(Tabbar);
Vue.use(TabbarItem);
Vue.use(Icon);
Vue.use(Search);
Vue.use(Lazyload );
Vue.use(Swipe );
Vue.use(SwipeItem );
Vue.use(Grid );
Vue.use(GridItem );
Vue.use(CountDown );
Vue.use(Tab );
Vue.use(Tabs );
Vue.use(Col );
Vue.use(Row );
Vue.use(Sticky );
Vue.use(Sidebar );
Vue.use(SidebarItem );
Vue.use(GoodsAction );
Vue.use(Popup );
Vue.use(GoodsActionIcon );
Vue.use(GoodsActionButton );
Vue.use(Toast );
Vue.use(Dialog );
Vue.use(Form );
Vue.use(Field );
Vue.use(Button );
Vue.use(Divider );