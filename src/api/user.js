import $ from "@/utils/requset"
// 首页数据
const getHomeApi = () => $.getAxios('homeApi.json')

// 注册
const postRegist = ({ tel = null, password = null } = {}) => $.postAxios('regist', { tel, password })
// 登录

const postLogin = ({ sms = null, yzm = null } = {}) => $.postAxios('login', { sms, yzm })
//分类
const leftList=()=>$.getAxios('homeApi/categories.json')
const like=(a)=>$.getAxios('homeApi/categoriesdetail/'+a)
const eat=()=>$.getAxios('recipe/allScene.json')
const eatlist=(a)=>$.getAxios('recipe/menulist/'+a)
export default {
    // 首页数据
    getHomeApi,
    //注册
    postRegist,
    //登录
    postLogin,
    //分类
    leftList,
    //猜你喜欢
    like,
    //吃什么
    eat,
    //吃什么内容数据
    eatlist
}