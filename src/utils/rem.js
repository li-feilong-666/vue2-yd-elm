export default (function(dou, win) {
        // orientationchange:判断当前移动端屏幕是否横竖屏（移动端提供事件）（监测当前设备是否更改）
        // 判断当前window中是否有orientationchange这个属性存在，如果有，则在移动端设备更改事件，否则为PC端resize事件，即窗口尺寸改变事件
        var resizeEvent = 'orientationchange' in window ? 'orientationchange' : 'resize';
        var docEL = document.documentElement;
        var foo = function() {
            // 获取当前设备的宽
            var clientWidth = docEL.clientWidth;
            //计算当前设备下根元素html的font-size的大小
            //假设设计稿的宽为750px 字体大小为1px 扩大100倍 100px 
            // 100px/设计稿的宽750px=？/当前设备的宽
            // 当前设备下html的font-size=(100*设备宽)/设计稿的宽750+'px'
            // 判断当前设备是否大于设计稿的宽，如果大于则强制设置当前根元素的font-size=100px否则进行计算
            if (!clientWidth) return;
            if (clientWidth >= 750) {
                docEL.style.fontSize = '100px';
            } else {
                docEL.style.fontSize = (100 * clientWidth) / 750 + 'px';
            }
    
        }
    
        win.addEventListener(resizeEvent, foo)
        dou.addEventListener('DOMContentLoaded', foo)
    })(document, window)
