import axios from "axios";
import { baseURL } from "./config";

//请求拦截 ====> 前端请求服务端数据时进行拦截（此时数据还没有传给服务端）
axios.interceptors.request.use( (config) => {
    //在发送请求之前进行拦截，可以进行token值的判断
    // console.log(config);
    if(localStorage.getItem('token')){
        config.headers.token = localStorage.getItem('token')
    }
    return config;
}, (err) => {
    //请求错误时弹框提示，或做些其他事
    return Promise.reject(err)
})
//响应拦截  ===> 服务端处理好数据之后把数据返回给前端，前端可以对数据进行处理
axios.interceptors.response.use( (res) => {
    // 对响应数据做点什么，允许在数据返回客户端前，修改响应的数据
    // 如果只需要返回体中数据res.data.data，则如下，如果需要全部，则 return res 即可
    return res;
}, (err) => {
    return Promise.reject(err)
})


//封装axios
function apiAxios (method, url, params) {
    let httpDefault = {
        method: method,
        baseURL: baseURL,
        url: url,
        // `params` 是即将与请求一起发送的 URL 参数
        // `data` 是作为请求主体被发送的数据
        params: method === 'GET' || method === 'DELETE' ? params : null,
        data: method === 'POST' || method === 'PUT' ? params : null,
        timeout: 10000
    }
    // 注意**Promise**使用(Promise首字母大写)
    return new Promise((resolve, reject) => {
        axios(httpDefault)
            // 此处的.then属于axios
            .then((res) => {
                //成功时返回的响应数据
                resolve(res)
            }).catch((response) => {
                // 失败时返回的响应数据
                reject(response)
            })
    })
}

export default {
    getAxios: (url,params) => apiAxios('GET',url,params) ,
    postAxios: (url, params) => apiAxios('POST', url, params),
}